﻿using Microsoft.AspNet.SignalR.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.IO.BACnet;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace HasIotLighting
{
    internal class Program
    {
        static BacnetClient client;
        private static MqttClient MqttClient = null;
        static int interv = 3000;
        static int interv_light = 7000;
        // All the present Bacnet Device List
        static List<BacNode> DevicesList = new List<BacNode>();
        private static IList<BacnetValue> value_list;
        private static MongoClient clientMongo = null;
        private static IMongoDatabase database = null;

        static HubConnection connection = null;
        static IHubProxy appHub = null;
        static string hubUrl = ConfigurationSettings.AppSettings["hubUrl"].ToString();

        private static void Main()
        {
            try
            {
                // SignalR connection
                initRTHub();

                string MQTTHost = ConfigurationManager.AppSettings["MQTTHost"].ToString();
                string PatchFile = ConfigurationManager.AppSettings["PatchFile"].ToString();
                string IPDevice = ConfigurationManager.AppSettings["IPDevice"].ToString();
                string mongoDBsvr = ConfigurationManager.AppSettings["mongodb_server"].ToString();

                // mongoDB connection 
                clientMongo = new MongoClient(mongoDBsvr);
                database = clientMongo.GetDatabase("IoT");

                // mqtt connection 
                MqttClient = new MqttClient(MQTTHost);
                MqttClient.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
                //MqttClient.Subscribe(new string[] { "Curtain/Bacnet/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                MqttClient.Subscribe(new string[] { "Lighting/Bacnet/Read" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                MqttClient.Subscribe(new string[] { "Lighting/Bacnet/Write" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                MqttClient.Subscribe(new string[] { "Lighting/Bacnet/Whois" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                MqttClient.Subscribe(new string[] { "Lighting/Bacnet/SetConfig" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                MqttClient.Subscribe(new string[] { "Lighting/Bacnet/GetConfig" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                MqttClient.Subscribe(new string[] { "Lighting/Bacnet/ConfigObject" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                MqttClient.Subscribe(new string[] { "Lighting/Bacnet/SetInterval" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                
                string clientId = Guid.NewGuid().ToString();
                MqttClient.Connect(clientId);

                void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
                {
                    var message = System.Text.Encoding.Default.GetString(e.Message);
                    if (IsValidJson(message))
                    {
                        string[] topic = e.Topic.Split('/');
                        string Category = topic[0];
                        string hostDevice = topic[1];
                        if (topic.Length > 1)
                        {
                            string cmd = topic[2];
                            switch (cmd)
                            {
                                case "Read":
                                    Thread threadRead = new Thread(()=> ReadToDevice(message));
                                    threadRead.Start();
                                    //ReadToDevice(message);
                                    break;
                                case "Write":
                                    Thread threadWrite = new Thread(() => WriteToDevice(message));
                                    threadWrite.Start();
                                    //WriteToDevice(message);
                                    break;
                                case "Whois":
                                    Thread threadwhois = new Thread(() => whoisDevice(message));
                                    threadwhois.Start();
                                    //whoisDevice(message);
                                    break;
                                case "SetConfig":
                                    setConfig(message);
                                    break;
                                case "GetConfig":
                                    getConfig(message);
                                    break;
                                case "ConfigObject":
                                    ConfigObject(message);
                                    break;
                                case "SetInterval":
                                    object result = JsonConvert.DeserializeObject(message);
                                    JObject voobj = JObject.Parse(result.ToString());
                                    break;
                                default:
                                    //Console.WriteLine("Other");
                                    break;
                            }
                        }
                    }
                }
                // aircond 192.168.105.201
                // power 10.0.1.209
                // power elv 10.0.2.6
                BacnetIpUdpProtocolTransport transport = new BacnetIpUdpProtocolTransport(0xBAC0, false, false, 1472, IPDevice);
                client = new BacnetClient(transport);
                client.OnIam += OnIAm;
                client.Start();
                client.WhoIs();

                //get data power from bacnet
                Thread t = new Thread(new ThreadStart(LightingIntv));
                t.Start();

                Thread l = new Thread(new ThreadStart(LUXIntv));
                l.Start();

                Thread d = new Thread(new ThreadStart(DimmIntv));
                d.Start();

                //Thread s = new Thread(new ThreadStart(SwitchedOffIntv));
                //s.Start();

                //send to ui
                //Thread s = new Thread(new ThreadStart(SendIntv));
                //s.Start();

            }
            catch (Exception e)
            {
                SendMail("The service Lighting is reported", e.Message);
                Console.WriteLine("Error : " + e.Message);
                Process currentProcess = Process.GetCurrentProcess();
                int pid = currentProcess.Id;
                string applicationName = currentProcess.ProcessName;
                RestartApp(pid, applicationName);
                System.Environment.Exit(1);
            }
        }
        
        public static async void LUXIntv()
        {
            string basepath = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
            if (File.Exists(basepath))
            {
                string Json = File.ReadAllText(basepath);
                if (Json != "null")
                {
                    object rootresult = JsonConvert.DeserializeObject(Json);
                    JObject rootobj = JObject.Parse(rootresult.ToString());
                    interv = Convert.ToInt32(rootobj["Interval"].ToString());
                }
            }
            bool ret;
            try
            {
                while (true)
                {
                    foreach (BacNode bn in DevicesList.ToList())
                    {
                        string poDevice = bn.device_id.ToString();
                        var builder = Builders<CurtainObjectWhois_REC>.Filter;

                        var DataObjectsLUX = database.GetCollection<CurtainObjectWhois_REC>("AllObjectDataWhois_LUX");
                        var builderLUX = Builders<CurtainObjectWhois_REC>.Filter;
                        var fltLuxObj = builder.And(builder.Eq<string>("DeviceID", poDevice), builderLUX.Eq<int>("RecordStatus", 1));
                        var vResultLUX = DataObjectsLUX.Find(fltLuxObj).ToList();

                        var SmartlightLUX = database.GetCollection<SmartlightLuxArea>("SmartlightLuxArea");
                        var SmartlightLUXZone = database.GetCollection<SmartlightLuxZone>("SmartlightLuxZone");

                        var pipelinelux = SmartlightLUX.Find(_ => true).ToList();

                        if (vResultLUX.Count > 0)
                        {
                            int voDevice = Convert.ToInt32(bn.device_id);
                            BacnetAddress adr = DeviceAddr((uint)voDevice);
                            int i = 0;

                            BacnetValue Value;
                            // All object data
                            foreach (CurtainObjectWhois_REC validObj in vResultLUX)
                            {
                                BacnetObjectTypes ObjType = (BacnetObjectTypes)Enum.Parse(typeof(BacnetObjectTypes), validObj.ObjectTypeName);
                                uint instance = Convert.ToUInt32(validObj.Instance);
                                var voObjId = new BacnetObjectId(ObjType, instance);
                                var objectName = await client.ReadPropertyAsync(adr, voObjId, BacnetPropertyIds.PROP_OBJECT_NAME);
                                var objectType = await client.ReadPropertyAsync(adr, voObjId, BacnetPropertyIds.PROP_OBJECT_TYPE);
                                ret = ReadScalarValue(voDevice, new BacnetObjectId(ObjType, instance), BacnetPropertyIds.PROP_PRESENT_VALUE, out Value);

                                if (ret)
                                {
                                    string ValueObj = Value.ToString();
                                    string ObjName = objectName[0].ToString();
                                    int TypeObj = Convert.ToInt32(objectType[0].ToString());

                                    // LUX Area
                                    foreach (SmartlightLuxArea data in pipelinelux)
                                    {
                                        var insteance_rec = data.Instance;
                                        if (insteance_rec == validObj.Instance)
                                        {
                                            if (Value.ToString() != "-1")
                                            {
                                                // LUX Zone

                                                var LuxBld = Builders<SmartlightLuxZone>.Filter;
                                                var fltBld = LuxBld.And(LuxBld.Eq<DateTime>("RecordTimestamp",
                                                    DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
                                                    LuxBld.Eq<int>("Instance", insteance_rec));
                                                var resLux = SmartlightLUXZone.Find(fltBld).Count();

                                                if (resLux == 0)
                                                {
                                                    SmartlightLuxZone volux = new SmartlightLuxZone();
                                                    volux.RecordTimestamp = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"));
                                                    volux.RecordStatus = 1;
                                                    volux.DeviceID = validObj.DeviceID.ToString();
                                                    volux.ObjectName = ObjName;
                                                    volux.Instance = validObj.Instance;
                                                    volux.PresentValue = Value.ToString();
                                                    volux.ObjectTypeID = TypeObj;
                                                    volux.ObjectTypeName = data.ObjectName;
                                                    volux.CategoryID = data.CategoryID;
                                                    volux.Category = data.Category;
                                                    volux.Plugin = validObj.Plugin;
                                                    volux.Times = Value.ToString();
                                                    SmartlightLUXZone.InsertOne(volux);
                                                }
                                                else
                                                {
                                                    var luxupdatebld = Builders<SmartlightLuxZone>.Filter;
                                                    var fltluxupdate = luxupdatebld.And(luxupdatebld.Eq<DateTime>("RecordTimestamp",
                                                        DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
                                                        luxupdatebld.Eq<int>("Instance", insteance_rec));
                                                    var resupdatelux = SmartlightLUXZone.Find(fltluxupdate).ToList();

                                                    string presentval = Value.ToString();
                                                    foreach (SmartlightLuxZone datalux in resupdatelux)
                                                    {
                                                        string sourceval = datalux.Times;
                                                        string[] stringseparators = new string[] { "," };
                                                        var resultval = sourceval.Split(stringseparators, StringSplitOptions.None);
                                                        int countval = resultval.Count();
                                                        double sum = 0.00;
                                                        for (int a = 0; a < countval; a++)
                                                        {
                                                            double num = double.Parse(resultval[a]);
                                                            sum = sum + num;
                                                        }
                                                        double avg = sum / countval;
                                                        var LXB = Builders<SmartlightLuxZone>.Filter;
                                                        var fltLXB = LXB.And(LXB.Eq<DateTime>("RecordTimestamp",
                                                            DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
                                                            LXB.Eq<int>("Instance", insteance_rec));
                                                        var updateLXB = Builders<SmartlightLuxZone>.Update
                                                            .Set("PresentValue", Math.Round(avg, 2).ToString())
                                                            .Set("Times", datalux.Times + "," + presentval);
                                                        SmartlightLUXZone.UpdateOne(fltLXB, updateLXB);
                                                    }
                                                }

                                                // Zone 2,3,4,11,12 (LUX Area)
                                                string presentval_lux = Value.ToString();
                                                var builderlux = Builders<SmartlightLuxArea>.Filter;
                                                var fltlux = builderlux.Eq<int>("Instance", validObj.Instance);
                                                var updatelux = Builders<SmartlightLuxArea>.Update.Set("RecordTimestamp", DateTime.Now)
                                                    .Set("PresentValue", presentval_lux);
                                                SmartlightLUX.UpdateOne(fltlux, updatelux);

                                            }
                                        }
                                    }

                                    var bldLg = Builders<HaystackPointView_REC>.Filter;
                                    string voID = "@" + voDevice.ToString() + "_" + validObj.Instance.ToString() + "_" + Convert.ToInt32(objectType[0].ToString());

                                    Console.WriteLine("Smartlight-LUX | Read | DeviceID : " + voID + " | curVal : " + Value.Value.ToString());
                                    ++i;
                                }
                            }
                        }
                    }
                    Thread.Sleep(interv);

                }
            }
            catch (Exception e)
            {
                SendMail("The service Curtain is reported", e.Message);
                Console.WriteLine("Error : " + e.Message);
                Process currentProcess = Process.GetCurrentProcess();
                int pid = currentProcess.Id;
                string applicationName = currentProcess.ProcessName;
                RestartApp(pid, applicationName);
                System.Environment.Exit(1);
            }
        }

        public static async void DimmIntv()
        {
            string basepath = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
            if (File.Exists(basepath))
            {
                string Json = File.ReadAllText(basepath);
                if (Json != "null")
                {
                    object rootresult = JsonConvert.DeserializeObject(Json);
                    JObject rootobj = JObject.Parse(rootresult.ToString());
                    interv = Convert.ToInt32(rootobj["Interval"].ToString());
                }
            }
            bool ret;
            try
            {
                while (true)
                {
                    foreach (BacNode bn in DevicesList.ToList())
                    {
                        string poDevice = bn.device_id.ToString();
                        var builder = Builders<CurtainObjectWhois_REC>.Filter;

                        var DataObjectsDimm = database.GetCollection<CurtainObjectWhois_REC>("AllObjectDataWhois_Dimming");
                        var builderDimm = Builders<CurtainObjectWhois_REC>.Filter;
                        var fltDimmObj = builder.And(builder.Eq<string>("DeviceID", poDevice), builderDimm.Eq<int>("RecordStatus", 1));
                        var vResultLUX = DataObjectsDimm.Find(fltDimmObj).ToList();

                        var CollectionSpecificZone = database.GetCollection<SmartlightSpecificZone>("SmartlightSpecificZone");

                        if (vResultLUX.Count > 0)
                        {
                            int voDevice = Convert.ToInt32(bn.device_id);
                            BacnetAddress adr = DeviceAddr((uint)voDevice);
                            int i = 0;

                            BacnetValue Value;
                            // All object data
                            foreach (CurtainObjectWhois_REC validObj in vResultLUX)
                            {
                                BacnetObjectTypes ObjType = (BacnetObjectTypes)Enum.Parse(typeof(BacnetObjectTypes), validObj.ObjectTypeName);
                                uint instance = Convert.ToUInt32(validObj.Instance);
                                var voObjId = new BacnetObjectId(ObjType, instance);
                                var objectName = await client.ReadPropertyAsync(adr, voObjId, BacnetPropertyIds.PROP_OBJECT_NAME);
                                var objectType = await client.ReadPropertyAsync(adr, voObjId, BacnetPropertyIds.PROP_OBJECT_TYPE);
                                ret = ReadScalarValue(voDevice, new BacnetObjectId(ObjType, instance), BacnetPropertyIds.PROP_PRESENT_VALUE, out Value);

                                if (ret)
                                {
                                    string ValueObj = Value.ToString();
                                    string ObjName = objectName[0].ToString();
                                    int TypeObj = Convert.ToInt32(objectType[0].ToString());

                                    var pipelinedimming = CollectionSpecificZone.Find(_ => true).ToList();

                                    // Dimming Value
                                    foreach (SmartlightSpecificZone dataDimming in pipelinedimming)
                                    {
                                        var bldSpecificon = Builders<SmartlightSpecificZone>.Filter;
                                        var flt_Specificon = bldSpecificon.Eq<int>("Instance", validObj.Instance);

                                        var update_Specificon = Builders<SmartlightSpecificZone>.Update.Set("DimmingValue", Value.ToString());
                                        CollectionSpecificZone.UpdateOne(flt_Specificon, update_Specificon);
                                    }

                                    ++i;
                                }
                            }
                        }
                    }
                    Thread.Sleep(interv_light);
                }
            }
            catch (Exception e)
            {
                SendMail("The service Curtain is reported", e.Message);
                Console.WriteLine("Error : " + e.Message);
                Process currentProcess = Process.GetCurrentProcess();
                int pid = currentProcess.Id;
                string applicationName = currentProcess.ProcessName;
                RestartApp(pid, applicationName);
                System.Environment.Exit(1);
            }
        }

        public static async void LightingIntv()
        {
            string basepath = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
            if (File.Exists(basepath))
            {
                string Json = File.ReadAllText(basepath);
                if (Json != "null")
                {
                    object rootresult = JsonConvert.DeserializeObject(Json);
                    JObject rootobj = JObject.Parse(rootresult.ToString());
                    interv = Convert.ToInt32(rootobj["Interval"].ToString());
                }
            }
            bool ret;
            try
            {
                while (true)
                {
                    foreach (BacNode bn in DevicesList.ToList())
                    {
                        string poDevice = bn.device_id.ToString();
                        var DataObjects = database.GetCollection<CurtainObjectWhois_REC>("AllObjectDataWhois");
                        var builder = Builders<CurtainObjectWhois_REC>.Filter;
                        var flt = builder.And(builder.Eq<string>("DeviceID", poDevice), builder.Eq<int>("RecordStatus", 1));
                        var vResult = DataObjects.Find(flt).ToList();

                        var DataLogs = database.GetCollection<Bacnet_REC>("CurtainDataLogs");
                        var DataLogsSmartlightBacnet = database.GetCollection<Bacnet_REC>("SmartlightBacnetDataLogs");
                        var SmartlightLUX = database.GetCollection<SmartlightLuxArea>("SmartlightLuxArea");
                        var SmartlightLUXZone = database.GetCollection<SmartlightLuxZone>("SmartlightLuxZone");
                        var SmartlightActiveDevices = database.GetCollection<SmartlightActiveDevices>("SmartlightActiveDevices");
                        var SwitchedOff = database.GetCollection<SmartlightSwitchedOff>("SmartlightSwitchedOff");
                        var CollectionSpecificZone = database.GetCollection<SmartlightSpecificZone>("SmartlightSpecificZone");
                        var CollectionPowerDataLogs = database.GetCollection<SmartlightPowerDataLogs>("SmartlightPowerDataLogs");

                        // Switchedoff
                        var DataObjectsOff = database.GetCollection<AllObjectWhoisSwitched_REC>("AllObjectDataWhois_SwitchedOff");
                        var builderOff = Builders<AllObjectWhoisSwitched_REC>.Filter;
                        var fltOff = builderOff.And(builderOff.Eq<string>("DeviceID", poDevice), builderOff.Eq<int>("RecordStatus", 1));
                        var vResultOff = DataObjectsOff.Find(fltOff).ToList();

                        var CollectionSwitchedOffhour = database.GetCollection<SmartlightSwitchedOffHourly>("SmartlightSwitchedOffHourly");

                        // Active Devices
                        if (vResult.Count > 0)
                        {
                            int voDevice = Convert.ToInt32(bn.device_id);
                            BacnetAddress adr = DeviceAddr((uint)voDevice);
                            int i = 0;

                            BacnetValue Value;
                            // All object data who is
                            foreach (CurtainObjectWhois_REC validObj in vResult)
                            {
                                BacnetObjectTypes ObjType = (BacnetObjectTypes)Enum.Parse(typeof(BacnetObjectTypes), validObj.ObjectTypeName);
                                uint instance = Convert.ToUInt32(validObj.Instance);
                                var voObjId = new BacnetObjectId(ObjType, instance);
                                var objectName = await client.ReadPropertyAsync(adr, voObjId, BacnetPropertyIds.PROP_OBJECT_NAME);
                                var objectType = await client.ReadPropertyAsync(adr, voObjId, BacnetPropertyIds.PROP_OBJECT_TYPE);
                                ret = ReadScalarValue(voDevice, new BacnetObjectId(ObjType, instance), BacnetPropertyIds.PROP_PRESENT_VALUE, out Value);

                                if (ret)
                                {
                                    string ValueObj = Value.ToString();
                                    string ObjName = objectName[0].ToString();
                                    int TypeObj = Convert.ToInt32(objectType[0].ToString());
                                    var pipelineactive = SmartlightActiveDevices.Find(_ => true).ToList();
                                    string presentval = Value.ToString();

                                    // Active Devices
                                    foreach (SmartlightActiveDevices data in pipelineactive)
                                    {
                                        var insteance_rec = data.Instance;
                                        if (insteance_rec == validObj.Instance)
                                        {
                                            if (Value.ToString() != "-1")
                                            {
                                            // Switched Off
                                                // Raw Data
                                                string objOffName = data.ObjectName;
                                                string objSuffix = objOffName.Substring(objOffName.Length - 13);
                                                if (objSuffix == "on/off Status")
                                                {
                                                    var pipelineOff = DataObjectsOff.Find(f => f.ObjectName == data.ObjectName).FirstOrDefault();
                                                    var BldOff = Builders<SmartlightSwitchedOffHourly>.Filter;
                                                    var flt_Off = BldOff.And(BldOff.Eq<DateTime>("RecordTimestamp",
                                                        DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
                                                        BldOff.Eq<string>("Location", pipelineOff.Location));
                                                    var resOff = CollectionSwitchedOffhour.Find(flt_Off).Count();

                                                    if (resOff == 0)
                                                    {
                                                        SmartlightSwitchedOffHourly voPD = new SmartlightSwitchedOffHourly();
                                                        voPD.RecordTimestamp = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"));
                                                        voPD.Location = pipelineOff.Location;
                                                        voPD.TotalOff = "0";
                                                        CollectionSwitchedOffhour.InsertOne(voPD);
                                                    }
                                                    // end raw Data

                                                    if (data.PresentValue == "1" && ValueObj == "0")
                                                    {
                                                        var BldOff_False = Builders<SmartlightSwitchedOffHourly>.Filter;
                                                        var flt_Off_False = BldOff_False.And(BldOff_False.Eq<DateTime>("RecordTimestamp",
                                                            DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
                                                            BldOff_False.Eq<string>("Location", pipelineOff.Location));
                                                        var Off_True = CollectionSwitchedOffhour.Find(flt_Off_False).FirstOrDefault();

                                                        int CurrentOff = Convert.ToInt32(Off_True.TotalOff);
                                                        int AppendOff = CurrentOff + 1;

                                                        var builder_on_true = Builders<SmartlightSwitchedOffHourly>.Filter;
                                                        var flt_on_true = builder_on_true.And(builder_on_true.Eq<DateTime>("RecordTimestamp", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
                                                        builder_on_true.Eq<string>("Location", pipelineOff.Location));

                                                        var update_on_true = Builders<SmartlightSwitchedOffHourly>.Update.Set("TotalOff", AppendOff.ToString());
                                                        CollectionSwitchedOffhour.UpdateOne(flt_on_true, update_on_true);
                                                    }
                                                }
                                            // end switchedoff

                                                // add smartlight power data log 
                                                var pdBld = Builders<SmartlightPowerDataLogs>.Filter;
                                                var pdflt = pdBld.And(pdBld.Eq<DateTime>("RecordTimestamp",
                                                    DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))),
                                                    pdBld.Eq<int>("Instance", insteance_rec));
                                                var resPD = CollectionPowerDataLogs.Find(pdflt).Count();

                                                if (resPD == 0)
                                                {
                                                    string objName = data.ObjectName; // "L7-L-180 on/off Status"
                                                    var PreObjName = objName.Remove(objName.Length - 14, 14);
                                                    var lightType = "Tube Power";

                                                    if (PreObjName == "L7-L-187" || PreObjName == "L7-L-188" || PreObjName == "L7-L-189" || PreObjName == "L7-L-190" || PreObjName == "L7-L-191" || PreObjName == "L7-L-192" ||
                                                        PreObjName == "L7-L-207" || PreObjName == "L7-L-208" ||
                                                        PreObjName == "L7-L-200" || PreObjName == "L7-L-201" ||
                                                        PreObjName == "L7-L-216" || PreObjName == "L7-L-217" ||
                                                        PreObjName == "L7-L-183" || PreObjName == "L7-L-184" || PreObjName == "L7-L-185" || PreObjName == "L7-L-186" || PreObjName == "L7-L-212" || PreObjName == "L7-L-213" ||
                                                        PreObjName == "L7-L-180" || PreObjName == "L7-L-181" || PreObjName == "L7-L-182" ||
                                                        PreObjName == "L7-L-177" || PreObjName == "L7-L-178" || PreObjName == "L7-L-179" ||
                                                        PreObjName == "L7-L-173" || PreObjName == "L7-L-174" || PreObjName == "L7-L-175" || PreObjName == "L7-L-176" ||
                                                        PreObjName == "L7-L-166" || PreObjName == "L7-L-167" || PreObjName == "L7-L-168" || PreObjName == "L7-L-169" || PreObjName == "L7-L-170" || PreObjName == "L7-L-171" || PreObjName == "L7-L-172" ||
                                                        PreObjName == "L7-L-160" || PreObjName == "L7-L-161" || PreObjName == "L7-L-162" || PreObjName == "L7-L-163" || PreObjName == "L7-L-164" || PreObjName == "L7-L-165" ||
                                                        PreObjName == "L7-L-197" || PreObjName == "L7-L-198" || PreObjName == "L7-L-199" ||
                                                        PreObjName == "L7-L-205")
                                                    {
                                                        lightType = "Downlight Power";
                                                    }
                                                    SmartlightPowerDataLogs voPD = new SmartlightPowerDataLogs();
                                                    voPD.RecordTimestamp = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"));
                                                    voPD.DeviceID = validObj.DeviceID;
                                                    voPD.ObjectName = validObj.ObjectName;
                                                    voPD.Category = data.Category;
                                                    voPD.Instance = validObj.Instance;
                                                    voPD.Lifetime = "0";
                                                    voPD.Power = "0";
                                                    voPD.RecordStatus = 1;
                                                    voPD.LightType = lightType;
                                                    CollectionPowerDataLogs.InsertOne(voPD);
                                                }
                                                // end power

                                                var builderactive = Builders<SmartlightActiveDevices>.Filter;
                                                var fltactive = builderactive.Eq<int>("Instance", validObj.Instance);

                                                // set time on / off
                                                if (data.PresentValue == presentval)
                                                {
                                                    // update on off status
                                                    var updateactive = Builders<SmartlightActiveDevices>.Update.Set("PresentValue", presentval);
                                                    SmartlightActiveDevices.UpdateOne(fltactive, updateactive);

                                                    if (data.PresentValue == "0" && presentval == "0")
                                                    {
                                                        var updateactive_false = Builders<SmartlightActiveDevices>.Update.Set("RecordTimestamp", DateTime.Now)
                                                            .Set("PresentValue", presentval);
                                                        SmartlightActiveDevices.UpdateOne(fltactive, updateactive_false);
                                                    }
                                                }
                                                else
                                                {
                                                    // lifetime power data log ---------------------------------------------------------------------------
                                                    if (data.PresentValue == "1" && presentval == "0") // switched off
                                                    {
                                                        var pdBld2 = Builders<SmartlightPowerDataLogs>.Filter;
                                                        var pdflt2 = pdBld2.And(pdBld2.Eq<DateTime>("RecordTimestamp",
                                                            DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))),
                                                            pdBld2.Eq<int>("Instance", insteance_rec));
                                                        var resPD2 = CollectionPowerDataLogs.Find(pdflt2).FirstOrDefault();

                                                        // update lifetime by day, power Kwh
                                                        // count minutes on
                                                        DateTime timestart = data.RecordTimestamp;
                                                        DateTime timeend = DateTime.Now;
                                                        double diffMinutes = timeend.Subtract(timestart).TotalMinutes;
                                                        double lifetimecurrent = double.Parse(resPD2.Lifetime);
                                                        double toHour = (((lifetimecurrent * 60) + diffMinutes)) / 60;
                                                        var totalHour = toHour.ToString();

                                                        // get power
                                                        double powerMeter = 42;
                                                        if (resPD2.LightType == "Downlight Power")
                                                        {
                                                            powerMeter = 15.5;
                                                        }
                                                        double totalPower = (powerMeter * toHour) / 1000;
                                                        // end get
                                                        var pd = Builders<SmartlightPowerDataLogs>.Filter;
                                                        var flt_pd = pd.And(pd.Eq<DateTime>("RecordTimestamp",
                                                            DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))),
                                                            pd.Eq<int>("Instance", insteance_rec));

                                                        var update_pd = Builders<SmartlightPowerDataLogs>.Update.Set("Lifetime", totalHour)
                                                            .Set("Power", totalPower.ToString());
                                                        CollectionPowerDataLogs.UpdateOne(flt_pd, update_pd);

                                                        // update timestamp
                                                        var ac1 = Builders<SmartlightActiveDevices>.Filter;
                                                        var fltac1 = ac1.And(ac1.Eq<DateTime>("RecordTimestamp", DateTime.Now),
                                                            ac1.Eq<int>("Instance", insteance_rec));
                                                        var updateact1 = Builders<SmartlightActiveDevices>.Update.Set("RecordTimestamp", DateTime.Now);
                                                        SmartlightActiveDevices.UpdateOne(fltac1, updateact1);
                                                    }
                                                    // end life time power data log ----------------------------------------------------------------------

                                                    // Lifetime Light Summary ----------------------------------------------------------------------------
                                                    string lighton = data.ObjectName;
                                                    var lightonsub = lighton.Remove(lighton.Length - 14, 14);
                                                    List<SmartlightSpecificZone> reslight = CollectionSpecificZone.Find(_ => true).ToList();
                                                    foreach (SmartlightSpecificZone dataLight in reslight)
                                                    {
                                                        string ls = dataLight.ObjectName;
                                                        var obls = ls.Remove(ls.Length - 13, 13);

                                                        if (obls == lightonsub)
                                                        {
                                                            if (presentval == "0")
                                                            {
                                                                // count minutes on
                                                                DateTime timestart = data.RecordTimestamp;
                                                                DateTime timeend = DateTime.Now;
                                                                double diffMinutes = timeend.Subtract(timestart).TotalMinutes;
                                                                double lifetimecurrent = double.Parse(dataLight.LifeTime);
                                                                double toHour = (((lifetimecurrent * 60) + diffMinutes)) / 60;
                                                                var totalHour = toHour.ToString();

                                                                var bldSpecificon = Builders<SmartlightSpecificZone>.Filter;
                                                                var flt_Specificon = bldSpecificon.Eq<int>("Instance", dataLight.Instance);

                                                                var update_Specificon = Builders<SmartlightSpecificZone>.Update.Set("LifeTime", totalHour);
                                                                CollectionSpecificZone.UpdateOne(flt_Specificon, update_Specificon);
                                                            }
                                                        }
                                                    }
                                                    // end lifetime -----------------------------------------------------------------------------

                                                    // update on off status
                                                    var updateactive = Builders<SmartlightActiveDevices>.Update.Set("RecordTimestamp", DateTime.Now)
                                                    .Set("PresentValue", presentval);
                                                    SmartlightActiveDevices.UpdateOne(fltactive, updateactive);
                                                }

                                                // Update Status on/off Specific Zone
                                                string lnson = data.ObjectName;
                                                var lightnamestatuson = lnson.Remove(lnson.Length - 14, 14);
                                                List<SmartlightSpecificZone> resSpecific = CollectionSpecificZone.Find(_ => true).ToList();
                                                foreach (SmartlightSpecificZone dataLight in resSpecific)
                                                {
                                                    string ls = dataLight.ObjectName;
                                                    var obls = ls.Remove(ls.Length - 13, 13);

                                                    if (obls == lightnamestatuson)
                                                    {
                                                        var bldSpecificon = Builders<SmartlightSpecificZone>.Filter;
                                                        var flt_Specificon = bldSpecificon.Eq<int>("Instance", dataLight.Instance);

                                                        var update_Specificon = Builders<SmartlightSpecificZone>.Update.Set("Relay", Value.ToString());
                                                        CollectionSpecificZone.UpdateOne(flt_Specificon, update_Specificon);
                                                    }
                                                }

                                                // Zone 2,3,4,11,12 (LUX Area)
                                                if (insteance_rec == 13010 || insteance_rec == 13000 || insteance_rec == 13017 || insteance_rec == 12995 || insteance_rec == 13004)
                                                {
                                                    string presentval_lux = Value.ToString();
                                                    string presentlux = "0.00";
                                                    if (presentval_lux == "0")
                                                    {
                                                        presentlux = "0.00";
                                                    }
                                                    else
                                                    {
                                                        presentlux = "500.00";
                                                    }
                                                    var builderlux = Builders<SmartlightLuxArea>.Filter;
                                                    var fltlux = builderlux.Eq<int>("Instance", validObj.Instance);
                                                    var updatelux = Builders<SmartlightLuxArea>.Update.Set("RecordTimestamp", DateTime.Now)
                                                        .Set("PresentValue", presentlux);
                                                    SmartlightLUX.UpdateOne(fltlux, updatelux);
                                                }
                                            }
                                        }
                                    }

                                    var bldLg = Builders<HaystackPointView_REC>.Filter;
                                    string voID = "@" + voDevice.ToString() + "_" + validObj.Instance.ToString() + "_" + Convert.ToInt32(objectType[0].ToString());

                                    Console.WriteLine("Smartlight | Read | DeviceID : " + voID + " | curVal : " + Value.Value.ToString());
                                    ++i;
                                }
                            }
                        }
                    }
                    Thread.Sleep(interv_light);

                }
            }
            catch (Exception e)
            {
                SendMail("The service Curtain is reported", e.Message);
                Console.WriteLine("Error : " + e.Message);
                Process currentProcess = Process.GetCurrentProcess();
                int pid = currentProcess.Id;
                string applicationName = currentProcess.ProcessName;
                RestartApp(pid, applicationName);
                System.Environment.Exit(1);
            }
        }

        public static async void SwitchedOffIntv()
        {
            string basepath = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
            if (File.Exists(basepath))
            {
                string Json = File.ReadAllText(basepath);
                if (Json != "null")
                {
                    object rootresult = JsonConvert.DeserializeObject(Json);
                    JObject rootobj = JObject.Parse(rootresult.ToString());
                    interv = Convert.ToInt32(rootobj["Interval"].ToString());
                }
            }
            bool ret;
            try
            {
                while (true)
                {
                    foreach (BacNode bn in DevicesList.ToList())
                    {
                        string poDevice = bn.device_id.ToString();
                        var DataObjects = database.GetCollection<AllObjectWhoisSwitched_REC>("AllObjectDataWhois_SwitchedOff");
                        var builder = Builders<AllObjectWhoisSwitched_REC>.Filter;
                        var flt = builder.And(builder.Eq<string>("DeviceID", poDevice), builder.Eq<int>("RecordStatus", 1));
                        var vResult = DataObjects.Find(flt).ToList();

                        var CollectionSwitchedOffhour = database.GetCollection<SmartlightSwitchedOffHourly>("SmartlightSwitchedOffHourly");
                        var SmartlightActiveDevices = database.GetCollection<SmartlightActiveDevices>("SmartlightActiveDevices");

                        DateTime lastUpdate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        var lastObjectUpdate = "";

                        if (vResult.Count > 0)
                        {
                            int voDevice = Convert.ToInt32(bn.device_id);
                            BacnetAddress adr = DeviceAddr((uint)voDevice);
                            int i = 0;

                            BacnetValue Value;
                            // All object data who is
                            foreach (AllObjectWhoisSwitched_REC validObj in vResult)
                            {
                                BacnetObjectTypes ObjType = (BacnetObjectTypes)Enum.Parse(typeof(BacnetObjectTypes), validObj.ObjectTypeName);
                                uint instance = Convert.ToUInt32(validObj.Instance);
                                var voObjId = new BacnetObjectId(ObjType, instance);
                                var objectName = await client.ReadPropertyAsync(adr, voObjId, BacnetPropertyIds.PROP_OBJECT_NAME);
                                var objectType = await client.ReadPropertyAsync(adr, voObjId, BacnetPropertyIds.PROP_OBJECT_TYPE);
                                ret = ReadScalarValue(voDevice, new BacnetObjectId(ObjType, instance), BacnetPropertyIds.PROP_PRESENT_VALUE, out Value);

                                if (ret)
                                {
                                    string ValueObj = Value.ToString();
                                    string ObjName = objectName[0].ToString();
                                    int TypeObj = Convert.ToInt32(objectType[0].ToString());
                                    var pipelineactive = SmartlightActiveDevices.Find(_ => true).ToList();
                                    var pipelineOff = CollectionSwitchedOffhour.Find(_ => true).ToList();

                                    // Active Devices
                                    foreach (SmartlightActiveDevices data in pipelineactive)
                                    {
                                        var insteance_rec = data.Instance;
                                        if (insteance_rec == validObj.Instance)
                                        {
                                            if (Value.ToString() != "-1")
                                            {
                                                // Switched Off
                                                // Raw Data
                                                var pdBld = Builders<SmartlightSwitchedOffHourly>.Filter;
                                                var pdflt = pdBld.And(pdBld.Eq<DateTime>("RecordTimestamp",
                                                    DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
                                                    pdBld.Eq<string>("Location", data.Category));
                                                var resPD = CollectionSwitchedOffhour.Find(pdflt).Count();

                                                if (resPD == 0)
                                                {
                                                    SmartlightSwitchedOffHourly voPD = new SmartlightSwitchedOffHourly();
                                                    voPD.RecordTimestamp = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"));
                                                    voPD.Location = data.Category;
                                                    voPD.TotalOff = "0";
                                                    CollectionSwitchedOffhour.InsertOne(voPD);
                                                }
                                                // end raw Data

                                                if (data.PresentValue == "1" && ValueObj == "0")
                                                {
                                                    DateTime timestart = lastUpdate;
                                                    DateTime timeend = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                                    double diffMinutes = timeend.Subtract(timestart).TotalSeconds;

                                                    if (lastObjectUpdate == data.ObjectName && diffMinutes <= 600)
                                                    {
                                                        //
                                                    }
                                                    else
                                                    {
                                                        var Off_True = CollectionSwitchedOffhour.Find(f => f.Location == data.Category).FirstOrDefault();
                                                        int CurrentOff = Convert.ToInt32(Off_True.TotalOff);
                                                        int AppendOff = CurrentOff + 1;

                                                        var builder_on_true = Builders<SmartlightSwitchedOffHourly>.Filter;
                                                        var flt_on_true = builder_on_true.And(builder_on_true.Eq<DateTime>("RecordTimestamp", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
                                                        builder_on_true.Eq<string>("Location", data.Category));

                                                        var update_on_true = Builders<SmartlightSwitchedOffHourly>.Update.Set("TotalOff", AppendOff.ToString());
                                                        CollectionSwitchedOffhour.UpdateOne(flt_on_true, update_on_true);

                                                        lastUpdate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                                        lastObjectUpdate = data.ObjectName;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    ++i;
                                }
                            }
                        }
                    }
                    Thread.Sleep(interv);

                }
            }
            catch (Exception e)
            {
                SendMail("The service Curtain is reported", e.Message);
                Console.WriteLine("Error : " + e.Message);
                Process currentProcess = Process.GetCurrentProcess();
                int pid = currentProcess.Id;
                string applicationName = currentProcess.ProcessName;
                RestartApp(pid, applicationName);
                System.Environment.Exit(1);
            }
        }
        //private static void ActiveDevices(CurtainObjectWhois_REC validObj,string Value)
        //{
        //    try
        //    {
        //        var DataLogs = database.GetCollection<Bacnet_REC>("CurtainDataLogs");
        //        var DataLogsSmartlightBacnet = database.GetCollection<Bacnet_REC>("SmartlightBacnetDataLogs");
        //        var SmartlightLUX = database.GetCollection<SmartlightLuxArea>("SmartlightLuxArea");
        //        var SmartlightLUXZone = database.GetCollection<SmartlightLuxZone>("SmartlightLuxZone");
        //        var SmartlightActiveDevices = database.GetCollection<SmartlightActiveDevices>("SmartlightActiveDevices");
        //        var SwitchedOff = database.GetCollection<SmartlightSwitchedOff>("SmartlightSwitchedOff");
        //        var CollectionSpecificZone = database.GetCollection<SmartlightSpecificZone>("SmartlightSpecificZone");
        //        var CollectionPowerDataLogs = database.GetCollection<SmartlightPowerDataLogs>("SmartlightPowerDataLogs");

        //        var pipelinelux = SmartlightLUX.Find(_ => true).ToList();
        //        var pipelineactive = SmartlightActiveDevices.Find(_ => true).ToList();
        //        var pipelinedimming = CollectionSpecificZone.Find(_ => true).ToList();
        //        var pipelinez10 = SmartlightActiveDevices.Find(x => x.Category == "F07Z10").ToList();

        //        // Active Devices
        //        foreach (SmartlightActiveDevices data in pipelineactive)
        //        {
        //            var insteance_rec = data.Instance;
        //            if (insteance_rec == validObj.Instance)
        //            {
        //                if (Value.ToString() != "-1")
        //                {
        //                    // add smartlight power data log 
        //                    var pdBld = Builders<SmartlightPowerDataLogs>.Filter;
        //                    var pdflt = pdBld.And(pdBld.Eq<DateTime>("RecordTimestamp",
        //                        DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))),
        //                        pdBld.Eq<int>("Instance", insteance_rec));
        //                    var resPD = CollectionPowerDataLogs.Find(pdflt).Count();

        //                    if (resPD == 0)
        //                    {
        //                        string objName = data.ObjectName; // "L7-L-180 on/off Status"
        //                        var PreObjName = objName.Remove(objName.Length - 14, 14);
        //                        var lightType = "Tube Power";

        //                        if (PreObjName == "L7-L-187" || PreObjName == "L7-L-188" || PreObjName == "L7-L-189" || PreObjName == "L7-L-190" || PreObjName == "L7-L-191" || PreObjName == "L7-L-192" ||
        //                            PreObjName == "L7-L-207" || PreObjName == "L7-L-208" ||
        //                            PreObjName == "L7-L-200" || PreObjName == "L7-L-201" ||
        //                            PreObjName == "L7-L-216" || PreObjName == "L7-L-217" ||
        //                            PreObjName == "L7-L-183" || PreObjName == "L7-L-184" || PreObjName == "L7-L-185" || PreObjName == "L7-L-186" || PreObjName == "L7-L-212" || PreObjName == "L7-L-213" ||
        //                            PreObjName == "L7-L-180" || PreObjName == "L7-L-181" || PreObjName == "L7-L-182" ||
        //                            PreObjName == "L7-L-177" || PreObjName == "L7-L-178" || PreObjName == "L7-L-179" ||
        //                            PreObjName == "L7-L-173" || PreObjName == "L7-L-174" || PreObjName == "L7-L-175" || PreObjName == "L7-L-176" ||
        //                            PreObjName == "L7-L-166" || PreObjName == "L7-L-167" || PreObjName == "L7-L-168" || PreObjName == "L7-L-169" || PreObjName == "L7-L-170" || PreObjName == "L7-L-171" || PreObjName == "L7-L-172" ||
        //                            PreObjName == "L7-L-160" || PreObjName == "L7-L-161" || PreObjName == "L7-L-162" || PreObjName == "L7-L-163" || PreObjName == "L7-L-164" || PreObjName == "L7-L-165" ||
        //                            PreObjName == "L7-L-197" || PreObjName == "L7-L-198" || PreObjName == "L7-L-199" ||
        //                            PreObjName == "L7-L-205")
        //                        {
        //                            lightType = "Downlight Power";
        //                        }
        //                        SmartlightPowerDataLogs voPD = new SmartlightPowerDataLogs();
        //                        voPD.RecordTimestamp = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"));
        //                        voPD.DeviceID = validObj.DeviceID;
        //                        voPD.ObjectName = validObj.ObjectName;
        //                        voPD.Category = data.Category;
        //                        voPD.Instance = validObj.Instance;
        //                        voPD.Lifetime = "0";
        //                        voPD.Power = "0";
        //                        voPD.RecordStatus = 1;
        //                        voPD.LightType = lightType;
        //                        CollectionPowerDataLogs.InsertOne(voPD);
        //                    }
        //                    // end power

        //                    string presentval = Value.ToString();
        //                    var builderactive = Builders<SmartlightActiveDevices>.Filter;
        //                    var fltactive = builderactive.Eq<int>("Instance", validObj.Instance);

        //                    // set time on / off
        //                    if (data.PresentValue == presentval)
        //                    {
        //                        // update on off status
        //                        var updateactive = Builders<SmartlightActiveDevices>.Update.Set("PresentValue", presentval);
        //                        SmartlightActiveDevices.UpdateOne(fltactive, updateactive);
        //                    }
        //                    else
        //                    {
        //                        // lifetime power data log ---------------------------------------------------------------------------
        //                        if (presentval == "0") // switched off
        //                        {
        //                            var pdBld2 = Builders<SmartlightPowerDataLogs>.Filter;
        //                            var pdflt2 = pdBld2.And(pdBld2.Eq<DateTime>("RecordTimestamp",
        //                                DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))),
        //                                pdBld2.Eq<int>("Instance", insteance_rec));
        //                            var resPD2 = CollectionPowerDataLogs.Find(pdflt2).FirstOrDefault();

        //                            // update lifetime by day, power Kwh
        //                            // count minutes on
        //                            DateTime timestart = data.RecordTimestamp;
        //                            DateTime timeend = DateTime.Now;
        //                            double diffMinutes = timeend.Subtract(timestart).TotalMinutes;
        //                            double lifetimecurrent = double.Parse(resPD2.Lifetime);
        //                            double toHour = (((lifetimecurrent * 60) + diffMinutes)) / 60;
        //                            var totalHour = toHour.ToString();

        //                            // get power
        //                            double powerMeter = 42;
        //                            if (resPD2.LightType == "Downlight Power")
        //                            {
        //                                powerMeter = 15.5;
        //                            }
        //                            double totalPower = (powerMeter * toHour) / 1000;
        //                            // end get
        //                            var pd = Builders<SmartlightPowerDataLogs>.Filter;
        //                            var flt_pd = pd.And(pd.Eq<DateTime>("RecordTimestamp",
        //                                DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"))),
        //                                pd.Eq<int>("Instance", insteance_rec));

        //                            var update_pd = Builders<SmartlightPowerDataLogs>.Update.Set("Lifetime", totalHour)
        //                                .Set("Power", totalPower.ToString());
        //                            CollectionPowerDataLogs.UpdateOne(flt_pd, update_pd);

        //                            // update timestamp
        //                            var ac1 = Builders<SmartlightActiveDevices>.Filter;
        //                            var fltac1 = ac1.And(ac1.Eq<DateTime>("RecordTimestamp", DateTime.Now),
        //                                ac1.Eq<int>("Instance", insteance_rec));
        //                            var updateact1 = Builders<SmartlightActiveDevices>.Update.Set("RecordTimestamp", DateTime.Now);
        //                            SmartlightActiveDevices.UpdateOne(fltac1, updateact1);
        //                        }
        //                        // end life time power data log ----------------------------------------------------------------------

        //                        // Lifetime Light Summary ----------------------------------------------------------------------------
        //                        string lighton = data.ObjectName;
        //                        var lightonsub = lighton.Remove(lighton.Length - 14, 14);
        //                        List<SmartlightSpecificZone> reslight = CollectionSpecificZone.Find(_ => true).ToList();
        //                        foreach (SmartlightSpecificZone dataLight in reslight)
        //                        {
        //                            string ls = dataLight.ObjectName;
        //                            var obls = ls.Remove(ls.Length - 13, 13);

        //                            if (obls == lightonsub)
        //                            {
        //                                if (presentval == "0")
        //                                {
        //                                    // count minutes on
        //                                    DateTime timestart = data.RecordTimestamp;
        //                                    DateTime timeend = DateTime.Now;
        //                                    double diffMinutes = timeend.Subtract(timestart).TotalMinutes;
        //                                    double lifetimecurrent = double.Parse(dataLight.LifeTime);
        //                                    double toHour = (((lifetimecurrent * 60) + diffMinutes)) / 60;
        //                                    var totalHour = toHour.ToString();

        //                                    var bldSpecificon = Builders<SmartlightSpecificZone>.Filter;
        //                                    var flt_Specificon = bldSpecificon.Eq<int>("Instance", dataLight.Instance);

        //                                    var update_Specificon = Builders<SmartlightSpecificZone>.Update.Set("LifeTime", totalHour);
        //                                    CollectionSpecificZone.UpdateOne(flt_Specificon, update_Specificon);
        //                                }
        //                            }
        //                        }
        //                        // end lifetime -----------------------------------------------------------------------------

        //                        // update on off status
        //                        var updateactive = Builders<SmartlightActiveDevices>.Update.Set("RecordTimestamp", DateTime.Now)
        //                        .Set("PresentValue", presentval);
        //                        SmartlightActiveDevices.UpdateOne(fltactive, updateactive);
        //                    }

        //                    // switchedoff

        //                    var soBld = Builders<SmartlightSwitchedOff>.Filter;
        //                    var soflt = soBld.And(soBld.Eq<DateTime>("RecordTimestamp",
        //                        DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
        //                        soBld.Eq<int>("Instance", insteance_rec));
        //                    var resSo = SwitchedOff.Find(soflt).Count();

        //                    if (resSo == 0)
        //                    {
        //                        SmartlightSwitchedOff voswitch = new SmartlightSwitchedOff();
        //                        voswitch.RecordTimestamp = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"));
        //                        voswitch.DeviceID = validObj.DeviceID;
        //                        voswitch.ObjectName = validObj.ObjectName;
        //                        voswitch.Location = data.Category;
        //                        voswitch.Instance = validObj.Instance;
        //                        voswitch.On = 0;
        //                        voswitch.Off = 0;
        //                        SwitchedOff.InsertOne(voswitch);

        //                        //update on
        //                        var builder_on_false = Builders<SmartlightSwitchedOff>.Filter;
        //                        var flt_on_false = builder_on_false.And(builder_on_false.Eq<DateTime>("RecordTimestamp", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
        //                        builder_on_false.Eq<int>("Instance", validObj.Instance));
        //                        var update_on_false = Builders<SmartlightSwitchedOff>.Update.Set("Off", presentval);
        //                        SwitchedOff.UpdateOne(flt_on_false, update_on_false);
        //                    }
        //                    else
        //                    {
        //                        var soBld_on = Builders<SmartlightSwitchedOff>.Filter;
        //                        var soflt_on = soBld_on.And(soBld.Eq<DateTime>("RecordTimestamp",
        //                            DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
        //                            soBld_on.Eq<int>("Instance", insteance_rec));
        //                        var resSo_on = SwitchedOff.Find(soflt).ToList();

        //                        foreach (SmartlightSwitchedOff dataon in resSo_on)
        //                        {

        //                            if (presentval == "1" && dataon.On == 1)
        //                            {
        //                                var statusoff_realtime = Int32.Parse(presentval);
        //                                var statusoff_before = dataon.Off;
        //                                var realtimeoff = statusoff_before + statusoff_realtime;

        //                                var builder_off = Builders<SmartlightSwitchedOff>.Filter;
        //                                var flt_off = builder_off.And(builder_off.Eq<DateTime>("RecordTimestamp",
        //                                DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),

        //                                builder_off.Eq<int>("Instance", validObj.Instance));
        //                                var update_off = Builders<SmartlightSwitchedOff>.Update.Set("Off", realtimeoff);
        //                                SwitchedOff.UpdateOne(flt_off, update_off);

        //                            }

        //                            if (presentval == "1" && dataon.On == 1)
        //                            {
        //                                var builder_on = Builders<SmartlightSwitchedOff>.Filter;
        //                                var flt_on = builder_on.And(builder_on.Eq<DateTime>("RecordTimestamp", DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
        //                                builder_on.Eq<int>("Instance", validObj.Instance));
        //                                var update_on = Builders<SmartlightSwitchedOff>.Update.Set("Off", 0);
        //                                SwitchedOff.UpdateOne(flt_on, update_on);
        //                            }
        //                        }
        //                    }
        //                    // Update Status on/off Specific Zone
        //                    string lnson = data.ObjectName;
        //                    var lightnamestatuson = lnson.Remove(lnson.Length - 14, 14);
        //                    List<SmartlightSpecificZone> resSpecific = CollectionSpecificZone.Find(_ => true).ToList();
        //                    foreach (SmartlightSpecificZone dataLight in resSpecific)
        //                    {
        //                        string ls = dataLight.ObjectName;
        //                        var obls = ls.Remove(ls.Length - 13, 13);

        //                        if (obls == lightnamestatuson)
        //                        {
        //                            var bldSpecificon = Builders<SmartlightSpecificZone>.Filter;
        //                            var flt_Specificon = bldSpecificon.Eq<int>("Instance", dataLight.Instance);

        //                            var update_Specificon = Builders<SmartlightSpecificZone>.Update.Set("Relay", Value.ToString());
        //                            CollectionSpecificZone.UpdateOne(flt_Specificon, update_Specificon);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        SendMail("The service Smartlight is reported", e.Message);
        //        Console.WriteLine("Error : " + e.Message);
        //        Process currentProcess = Process.GetCurrentProcess();
        //        int pid = currentProcess.Id;
        //        string applicationName = currentProcess.ProcessName;
        //        RestartApp(pid, applicationName);
        //        System.Environment.Exit(1);
        //    }
        //}

        //private static void LuxArea(CurtainObjectWhois_REC validObj,string Value, string ObjName, int ObjType)
        //{
        //    try
        //    {
        //        var DataLogs = database.GetCollection<Bacnet_REC>("CurtainDataLogs");
        //        var DataLogsSmartlightBacnet = database.GetCollection<Bacnet_REC>("SmartlightBacnetDataLogs");
        //        var SmartlightLUX = database.GetCollection<SmartlightLuxArea>("SmartlightLuxArea");
        //        var SmartlightLUXZone = database.GetCollection<SmartlightLuxZone>("SmartlightLuxZone");
        //        var SmartlightActiveDevices = database.GetCollection<SmartlightActiveDevices>("SmartlightActiveDevices");
        //        var SwitchedOff = database.GetCollection<SmartlightSwitchedOff>("SmartlightSwitchedOff");
        //        var CollectionSpecificZone = database.GetCollection<SmartlightSpecificZone>("SmartlightSpecificZone");
        //        var CollectionPowerDataLogs = database.GetCollection<SmartlightPowerDataLogs>("SmartlightPowerDataLogs");

        //        var pipelinelux = SmartlightLUX.Find(_ => true).ToList();
        //        var pipelineactive = SmartlightActiveDevices.Find(_ => true).ToList();
        //        var pipelinedimming = CollectionSpecificZone.Find(_ => true).ToList();
        //        var pipelinez10 = SmartlightActiveDevices.Find(x => x.Category == "F07Z10").ToList();

        //        // LUX Area
        //        foreach (SmartlightLuxArea data in pipelinelux)
        //        {
        //            var insteance_rec = data.Instance;
        //            if (insteance_rec == validObj.Instance)
        //            {
        //                if (Value.ToString() != "-1")
        //                {
        //                    // LUX Zone

        //                    var LuxBld = Builders<SmartlightLuxZone>.Filter;
        //                    var fltBld = LuxBld.And(LuxBld.Eq<DateTime>("RecordTimestamp",
        //                        DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
        //                        LuxBld.Eq<int>("Instance", insteance_rec));
        //                    var resLux = SmartlightLUXZone.Find(fltBld).Count();

        //                    if (resLux == 0)
        //                    {
        //                        SmartlightLuxZone volux = new SmartlightLuxZone();
        //                        volux.RecordTimestamp = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"));
        //                        volux.RecordStatus = 1;
        //                        volux.DeviceID = validObj.DeviceID.ToString();
        //                        volux.ObjectName = ObjName;
        //                        volux.Instance = validObj.Instance;
        //                        volux.PresentValue = Value.ToString();
        //                        volux.ObjectTypeID = ObjType;
        //                        volux.ObjectTypeName = data.ObjectName;
        //                        volux.CategoryID = data.CategoryID;
        //                        volux.Category = data.Category;
        //                        volux.Plugin = validObj.Plugin;
        //                        volux.Times = Value.ToString();
        //                        SmartlightLUXZone.InsertOne(volux);
        //                    }
        //                    else
        //                    {
        //                        var luxupdatebld = Builders<SmartlightLuxZone>.Filter;
        //                        var fltluxupdate = luxupdatebld.And(luxupdatebld.Eq<DateTime>("RecordTimestamp",
        //                            DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
        //                            luxupdatebld.Eq<int>("Instance", insteance_rec));
        //                        var resupdatelux = SmartlightLUXZone.Find(fltluxupdate).ToList();

        //                        string presentval = Value.ToString();
        //                        foreach (SmartlightLuxZone datalux in resupdatelux)
        //                        {
        //                            string sourceval = datalux.Times;
        //                            string[] stringseparators = new string[] { "," };
        //                            var resultval = sourceval.Split(stringseparators, StringSplitOptions.None);
        //                            int countval = resultval.Count();
        //                            double sum = 0.00;
        //                            for (int a = 0; a < countval; a++)
        //                            {
        //                                double num = double.Parse(resultval[a]);
        //                                sum = sum + num;
        //                            }
        //                            double avg = sum / countval;
        //                            var LXB = Builders<SmartlightLuxZone>.Filter;
        //                            var fltLXB = LXB.And(LXB.Eq<DateTime>("RecordTimestamp",
        //                                DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:00:00"))),
        //                                LXB.Eq<int>("Instance", insteance_rec));
        //                            var updateLXB = Builders<SmartlightLuxZone>.Update
        //                                .Set("PresentValue", Math.Round(avg, 2).ToString())
        //                                .Set("Times", datalux.Times + "," + presentval);
        //                            SmartlightLUXZone.UpdateOne(fltLXB, updateLXB);
        //                        }
        //                    }

        //                    // Zone 2,3,4,11,12 (LUX Area)
        //                    if (insteance_rec == 13010 || insteance_rec == 13000 || insteance_rec == 13017 || insteance_rec == 12995 || insteance_rec == 13004)
        //                    {
        //                        string presentval = Value.ToString();
        //                        string presentlux = "0.00";
        //                        if (presentval == "0")
        //                        {
        //                            presentlux = "0.00";
        //                        }
        //                        else
        //                        {
        //                            presentlux = "500.00";
        //                        }
        //                        var builderlux = Builders<SmartlightLuxArea>.Filter;
        //                        var fltlux = builderlux.Eq<int>("Instance", validObj.Instance);
        //                        var updatelux = Builders<SmartlightLuxArea>.Update.Set("RecordTimestamp", DateTime.Now)
        //                            .Set("PresentValue", presentlux);
        //                        SmartlightLUX.UpdateOne(fltlux, updatelux);
        //                    }
        //                    else
        //                    {
        //                        string presentval = Value.ToString();
        //                        var builderlux = Builders<SmartlightLuxArea>.Filter;
        //                        var fltlux = builderlux.Eq<int>("Instance", validObj.Instance);
        //                        var updatelux = Builders<SmartlightLuxArea>.Update.Set("RecordTimestamp", DateTime.Now)
        //                            .Set("PresentValue", presentval);
        //                        SmartlightLUX.UpdateOne(fltlux, updatelux);
        //                    }

        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        SendMail("The service Smartlight is reported", e.Message);
        //        Console.WriteLine("Error : " + e.Message);
        //        Process currentProcess = Process.GetCurrentProcess();
        //        int pid = currentProcess.Id;
        //        string applicationName = currentProcess.ProcessName;
        //        RestartApp(pid, applicationName);
        //        System.Environment.Exit(1);
        //    }
        //}

        //private static void DimmingValue(CurtainObjectWhois_REC validObj, string Value)
        //{
        //    try
        //    {
        //        var CollectionSpecificZone = database.GetCollection<SmartlightSpecificZone>("SmartlightSpecificZone");
        //        var pipelinedimming = CollectionSpecificZone.Find(_ => true).ToList();

        //        // Dimming Value
        //        foreach (SmartlightSpecificZone data in pipelinedimming)
        //        {
        //            var bldSpecificon = Builders<SmartlightSpecificZone>.Filter;
        //            var flt_Specificon = bldSpecificon.Eq<int>("Instance", validObj.Instance);

        //            var update_Specificon = Builders<SmartlightSpecificZone>.Update.Set("DimmingValue", Value.ToString());
        //            CollectionSpecificZone.UpdateOne(flt_Specificon, update_Specificon);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        SendMail("The service Smartlight is reported", e.Message);
        //        Console.WriteLine("Error : " + e.Message);
        //        Process currentProcess = Process.GetCurrentProcess();
        //        int pid = currentProcess.Id;
        //        string applicationName = currentProcess.ProcessName;
        //        RestartApp(pid, applicationName);
        //        System.Environment.Exit(1);
        //    }
        //}


        static void RestartApp(int pid, string applicationName)
        {
            // Wait for the process to terminate
            Process process = null;
            try
            {
                process = Process.GetProcessById(pid);
                process.WaitForExit(1000);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            Process.Start(applicationName, "");
            SendMail("The service Curtain is reported", "Restarting application success");
        }
        private static async void OnIAm(BacnetClient sender, BacnetAddress adr,
            uint deviceid, uint maxapdu, BacnetSegmentations segmentation, ushort vendorid)
        {
            try
            {
                Console.WriteLine($"Detected device {deviceid} at {adr}");

                lock (DevicesList)
                {
                    // Device already registred ?
                    foreach (BacNode bn in DevicesList)
                        if (bn.getAdd(deviceid) != null) return;   // Yes

                    // Not already in the list
                    DevicesList.Add(new BacNode(adr, deviceid));   // add it
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : " + ex.Message);
            }
        }
        private static void setConfig(string message)
        {
            object result = JsonConvert.DeserializeObject(message);
            JObject voobj = JObject.Parse(result.ToString());
            string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";

            Dictionary<string, string> voResData = new Dictionary<string, string>();
            voResData.Add("MQTTHOST", voobj["MQTTHOST"].ToString());
            voResData.Add("PatchFile", voobj["PatchFile"].ToString());
            voResData.Add("Interval", voobj["Interval"].ToString());

            // begin write to json file
            string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
            File.WriteAllText(basepatch, strValue);
            // end wriet to json file

            // publish mqtt
            MqttClient.Publish("Curtain/Bacnet/All/setConfigData", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            // Restart Apps
            Process currentProcess = Process.GetCurrentProcess();
            int pid = currentProcess.Id;
            string applicationName = currentProcess.ProcessName;
            RestartApp(pid, applicationName);
            System.Environment.Exit(1);
        }
        private static void ConfigObject(string message)
        {
            object result = JsonConvert.DeserializeObject(message);
            JObject voobj = JObject.Parse(result.ToString());

            string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
            if (File.Exists(basepatch))
            {
                string Json = File.ReadAllText(basepatch);
                if (Json != "null")
                {
                    object rootresult = JsonConvert.DeserializeObject(Json);
                    JObject rootobj = JObject.Parse(rootresult.ToString());
                    string poMQTT = rootobj["MQTTHOST"].ToString();
                    string poPatch = rootobj["PatchFile"].ToString();
                    string poIntv = rootobj["Interval"].ToString();
                    // publish mqtt
                    Dictionary<string, string> voResData = new Dictionary<string, string>();
                    voResData.Add("MQTTHOST", poMQTT);
                    voResData.Add("PatchFile", poPatch);
                    voResData.Add("Interval", poIntv);
                    string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                    MqttClient.Publish("Curtain/Bacnet/All/getConfigData", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
            }
        }
        private static void getConfig(string message)
        {
            object result = JsonConvert.DeserializeObject(message);
            JObject voobj = JObject.Parse(result.ToString());

            string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
            if (File.Exists(basepatch))
            {
                string Json = File.ReadAllText(basepatch);
                if (Json != "null")
                {
                    object rootresult = JsonConvert.DeserializeObject(Json);
                    JObject rootobj = JObject.Parse(rootresult.ToString());
                    string poMQTT = rootobj["MQTTHOST"].ToString();
                    string poPatch = rootobj["PatchFile"].ToString();
                    string poIntv = rootobj["Interval"].ToString();
                    // publish mqtt
                    Dictionary<string, string> voResData = new Dictionary<string, string>();
                    voResData.Add("MQTTHOST", poMQTT);
                    voResData.Add("PatchFile", poPatch);
                    voResData.Add("Interval", poIntv);
                    string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                    MqttClient.Publish("Curtain/Bacnet/All/getConfigData", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
            }
        }
        private static async void whoisDevice(string message)
        {
            Dictionary<int, Dictionary<string, string>> ResData = new Dictionary<int, Dictionary<string, string>>();
            object result = JsonConvert.DeserializeObject(message);
            JObject voobj = JObject.Parse(result.ToString());
            uint deviceid = Convert.ToUInt32(voobj["deviceID"].ToString());
            BacnetAddress adr;

            try
            {
                // Looking for the device
                adr = DeviceAddr((uint)deviceid);
                if (adr == null) { throw new Exception("Device not found or disconect from network"); }

                var deviceObjId = new BacnetObjectId(BacnetObjectTypes.OBJECT_DEVICE, deviceid);
                //var objectIdList = await client.ReadPropertyAsync(adr, deviceObjId, BacnetPropertyIds.PROP_OBJECT_LIST);

                var objectIdList = new LinkedList<BacnetObjectId>();
                client.ReadPropertyRequest(adr, deviceObjId, BacnetPropertyIds.PROP_OBJECT_LIST, out value_list, arrayIndex: 0);
                var objectCount = value_list.First().As<uint>();

                for (uint a = 1; a <= objectCount; a++)
                {
                    client.ReadPropertyRequest(adr, deviceObjId, BacnetPropertyIds.PROP_OBJECT_LIST, out value_list, arrayIndex: a);
                    objectIdList.AddLast(value_list.First().As<BacnetObjectId>());
                }

                Dictionary<string, string> ResDataObjName = new Dictionary<string, string>();
                int i = 0;
                foreach (var objId in objectIdList)
                {
                    Dictionary<string, string> ResDataObj = new Dictionary<string, string>();
                    //Console.WriteLine($"{objId}");
                    string OBJ = objId.ToString();

                    string[] oi = OBJ.Split(':');
                    uint instance = Convert.ToUInt32(oi[1]);
                    BacnetObjectTypes ObjType = (BacnetObjectTypes)Enum.Parse(typeof(BacnetObjectTypes), oi[0]);
                    var voObjId = new BacnetObjectId(ObjType, instance);
                    var objectName = await client.ReadPropertyAsync(adr, voObjId, BacnetPropertyIds.PROP_OBJECT_NAME);
                    var objectIdentifier = await client.ReadPropertyAsync(adr, voObjId, BacnetPropertyIds.PROP_OBJECT_IDENTIFIER);
                    var objectType = await client.ReadPropertyAsync(adr, voObjId, BacnetPropertyIds.PROP_OBJECT_TYPE);
                    //var objectDesc = await client.ReadPropertyAsync(adr, voObjId, BacnetPropertyIds.PROP_DESCRIPTION);
                    ResDataObj.Add("ObjType", OBJ);
                    ResDataObj.Add("ObjName", objectName[0].ToString());
                    ResDataObj.Add("objectIdentifier", objectIdentifier[0].ToString());
                    ResDataObj.Add("objectType", objectType[0].ToString());
                    //ResDataObj.Add("objectDesc", objectDesc[0].ToString());
                    ResData.Add(i, ResDataObj);
                    i++;
                }
                // publish to mqtt
                string strValue = Convert.ToString(JsonConvert.SerializeObject(ResData));
                MqttClient.Publish("Curtain/Bacnet/" + deviceid + "/WhoisDatas", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            catch (Exception e)
            {
                SendMail("The service Curtain is reported", e.Message);
                Dictionary<string, string> voResData = new Dictionary<string, string>();
                voResData.Add("deviceID", voobj["deviceID"].ToString());
                voResData.Add("msg", e.Message);
                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                MqttClient.Publish("Curtain/Bacnet/" + deviceid + "/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }

        }
        static void ReadToDevice(string message)
        {
            object result = JsonConvert.DeserializeObject(message);
            JObject voobj = JObject.Parse(result.ToString());
            try
            {
                BacnetValue Value;
                bool ret;
                int DeviceID = Convert.ToInt32(voobj["deviceID"].ToString());
                string[] poObj = voobj["ObjectType"].ToString().Split(':');
                uint instance = Convert.ToUInt32(poObj[1]);
                BacnetObjectTypes ObjType = (BacnetObjectTypes)Enum.Parse(typeof(BacnetObjectTypes), poObj[0]);
                ret = ReadScalarValue(DeviceID, new BacnetObjectId(ObjType, instance), BacnetPropertyIds.PROP_PRESENT_VALUE, out Value);
                if (ret == true)
                {
                    //Console.WriteLine("Read (" + DeviceID + ") value : " + Value.Value.ToString());
                    // publish to mqtt
                    string strValue = "{\"deviceID\":\"" + DeviceID + "\",\"ObjectType\":\"" + voobj["ObjectType"].ToString() + "\",\"value\":\"" + Value.Value.ToString() + "\"}";
                    //heandlerSignalR("WidgetRealtime", "AirconRead", strValue);
                    //MqttClient.Publish("Curtain/Bacnet/" + DeviceID + "/ReadDatas", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                    MqttClient.Publish("Lighting/Bacnet/" + DeviceID + "/MobileRead", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
            }
            catch (Exception e)
            {
                SendMail("The service Curtain is reported", e.Message);
                Dictionary<string, string> voResData = new Dictionary<string, string>();
                voResData.Add("deviceID", voobj["deviceID"].ToString());
                voResData.Add("msg", e.Message);
                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                MqttClient.Publish("Lighting/Bacnet/" + voobj["deviceID"].ToString() + "/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
        }
        static void WriteToDevice(string message)
        {

            object result = JsonConvert.DeserializeObject(message);
            JObject voobj = JObject.Parse(result.ToString());
            try
            {
                var LogData = database.GetCollection<CurtainLogService>("CurtainLogService");


                bool ret;
                int DeviceID = Convert.ToInt32(voobj["deviceID"].ToString());
                string[] poObj = voobj["ObjectType"].ToString().Split(':');
                uint instance = Convert.ToUInt32(poObj[1]);
                BacnetObjectTypes ObjType = (BacnetObjectTypes)Enum.Parse(typeof(BacnetObjectTypes), poObj[0]);
                int voValue = Convert.ToInt32(voobj["value"].ToString());

                CurtainLogService voLog = new CurtainLogService();
                voLog.RecordTimestamp = DateTime.Now;
                voLog.DeviceID = DeviceID.ToString();
                voLog.ObjectName = poObj[0].ToString();
                voLog.PresentValue = voValue.ToString();
                voLog.InstanceID = poObj[1];
                voLog.Status = "Writing";
                voLog.ObjectTypeID = Convert.ToInt32(poObj[1].ToString());
                voLog.ObjectType = poObj[0];
                LogData.InsertOne(voLog);

                if (poObj[0] == "OBJECT_ANALOG_VALUE")
                {
                    BacnetValue newValue = new BacnetValue(Convert.ToSingle(voValue));
                    ret = WriteScalarValue(DeviceID, new BacnetObjectId(ObjType, instance), BacnetPropertyIds.PROP_PRESENT_VALUE, newValue);
                }
                else
                {
                    BacnetValue newValue = new BacnetValue(BacnetApplicationTags.BACNET_APPLICATION_TAG_ENUMERATED, voValue);
                    ret = WriteScalarValue(DeviceID, new BacnetObjectId(ObjType, instance), BacnetPropertyIds.PROP_PRESENT_VALUE, newValue);

                }

                voLog.RecordTimestamp = DateTime.Now;
                voLog.Status = "Write result: " + ret.ToString();
                LogData.InsertOne(voLog);

                if (ret == true)
                {
                    Console.WriteLine("Write feedback : " + ret.ToString());
                    // publish to mqtt
                    string strValue = "{\"deviceID\":\"" + DeviceID + "\",\"ObjectType\":\"" + voobj["ObjectType"].ToString() + "\",\"value\":\"" + ret.ToString() + "\"}";


                    if (poObj[1] == "28" || poObj[1] == "1799" || poObj[1] == "8193")
                    {
                        string strValueCurtain = "{\"deviceID\":\"" + DeviceID + "\",\"ObjectType\":\"" + voobj["ObjectType"].ToString() + "\",\"InstanceID\":\"" + poObj[1].ToString() + "\",\"PresentValue\":\"" + voValue + "\"}";
                        heandlerSignalR("WidgetRealtime", "CurtainStatus", strValueCurtain);
                    }

                    // 4101 (Close Curtain 1)
                    // 4103 (Close Curtain 2)
                    // 4097 (Pause Curtain 1)
                    // 4099 (Pause Curtain 2)
                    // 4100 (Open Curtain 1)
                    // 4102 (Open Curtain 2)
                    if (poObj[1] == "4101" || poObj[1] == "4103" || poObj[1] == "4097" || poObj[1] == "4099" || poObj[1] == "4100" || poObj[1] == "4102")
                    {
                        string strValueCurtain = "{\"deviceID\":\"" + DeviceID + "\",\"ObjectType\":\"" + voobj["ObjectType"].ToString() + "\",\"InstanceID\":\"" + poObj[1].ToString() + "\",\"PresentValue\":\"" + voValue + "\"}";
                        heandlerSignalR("WidgetRealtime", "CurtainStatus", strValueCurtain);
                    }

                    MqttClient.Publish("Lighting/Bacnet/" + DeviceID + "/WriteDatas", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }

                voLog.RecordTimestamp = DateTime.Now;
                voLog.Status = "Finish";
                LogData.InsertOne(voLog);
            }
            catch (Exception e)
            {
                SendMail("The service Curtain is reported", e.Message);
                Dictionary<string, string> voResData = new Dictionary<string, string>();
                voResData.Add("deviceID", voobj["deviceID"].ToString());
                voResData.Add("msg", e.Message);
                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                MqttClient.Publish("Lighting/Bacnet/" + voobj["deviceID"].ToString() + "/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
        }
        static bool ReadScalarValue(int device_id, BacnetObjectId BacnetObjet, BacnetPropertyIds Propriete, out BacnetValue Value)
        {
            BacnetAddress adr;
            IList<BacnetValue> NoScalarValue;

            Value = new BacnetValue(null);

            // Looking for the device
            adr = DeviceAddr((uint)device_id);
            if (adr == null) return false;  // not found

            // Property Read
            if (client.ReadPropertyRequest(adr, BacnetObjet, Propriete, out NoScalarValue) == false)
                return false;

            Value = NoScalarValue[0];
            return true;
        }
        static bool WriteScalarValue(int device_id, BacnetObjectId BacnetObjet, BacnetPropertyIds Propriete, BacnetValue Value)
        {
            BacnetAddress adr;

            // Looking for the device
            adr = DeviceAddr((uint)device_id);
            if (adr == null) return false;  // not found

            // Property Write
            BacnetValue[] NoScalarValue = { Value };
            if (client.WritePropertyRequest(adr, BacnetObjet, Propriete, NoScalarValue) == false)
                return false;

            return true;
        }
        static BacnetAddress DeviceAddr(uint device_id)
        {
            BacnetAddress ret;

            lock (DevicesList)
            {
                foreach (BacNode bn in DevicesList)
                {
                    ret = bn.getAdd(device_id);
                    if (ret != null) return ret;
                }
                // not in the list
                return null;
            }
        }
        public static bool IsValidJson(string strInput)
        {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private static void initRTHub()
        {
            try
            {
                //Set connection
                connection = new HubConnection(hubUrl);
                //Make proxy to hub based on hub name on server
                appHub = connection.CreateHubProxy("apphub");
                //Start connection
                connection.Start().ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        task.Exception.GetBaseException();
                        Console.WriteLine(task.Exception.GetBaseException());
                    }
                    else
                    {
                        Console.WriteLine("Connected");
                    }
                }).Wait();
            }
            catch (Exception ex)
            {
                SendMail("The service Curtain is reported", "init signalR " + ex.Message);
                Console.WriteLine("ex init signalR " + ex.Message);
            }
        }
        private static void heandlerSignalR(string mod, string tp, string sp)
        {
            try
            {
                appHub.Invoke<string>("Send", mod, tp, sp).ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        Console.WriteLine("There was an error calling send: {0}", task.Exception.GetBaseException());
                        if (connection.State == ConnectionState.Disconnected)
                        {
                            initRTHub();
                            heandlerSignalR(mod, tp, sp);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Result Handler signalR " + task.Result);
                    }
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("ex Handler signalR " + ex.Message);
                SendMail("The service Curtain is reported", "init signalR " + ex.Message);
                if (connection.State == ConnectionState.Disconnected)
                {
                    initRTHub();
                    heandlerSignalR(mod, tp, sp);
                }
            }
        }

        #region mail sender
        private static void SendMail(string subject, string message)
        {
            try
            {
                var mailData = database.GetCollection<MailServer_REC>("MailServer");
                var bd = Builders<MailServer_REC>.Filter.Empty;
                var voDt = mailData.Find(bd).SingleOrDefault();
                if (voDt != null)
                {
                    string from = voDt.emailAddress;
                    var fromAddr = new MailAddress(from, voDt.title);
                    string[] receivers = voDt.to.Split(',');
                    string[] receiversCC = voDt.cc.Split(',');
                    string[] receiversBCC = voDt.bcc.Split(',');
                    var toAddr = new MailAddress(receivers[0]);
                    var client = new SmtpClient
                    {
                        Host = voDt.host,
                        Port = voDt.port,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Timeout = 30 * 1000,
                        Credentials = new System.Net.NetworkCredential(fromAddr.Address, voDt.password)
                    };
                    using (var msg = new MailMessage(fromAddr, toAddr))
                    {
                        for (int i = 1; i <= receivers.Count() - 1; i++)
                        {
                            msg.To.Add(receivers[i]);
                        }
                        if (receiversCC[0] != "")
                        {
                            foreach (string adrCC in receiversCC)
                            {
                                msg.CC.Add(adrCC);
                            }
                        }
                        if (receiversBCC[0] != "")
                        {
                            foreach (string adrBCC in receiversBCC)
                            {
                                msg.Bcc.Add(adrBCC);
                            }
                        }
                        msg.Subject = subject;
                        msg.Body = message;
                        client.Send(msg);
                    }
                }
            }
            catch (Exception e)
            {
                Dictionary<string, string> ResDataObj = new Dictionary<string, string>();
                ResDataObj.Add("msg", e.Message);
                string strValue = Convert.ToString(JsonConvert.SerializeObject(ResDataObj));
                MqttClient.Publish("Power/Bacnet/All/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }

        }
        #endregion

    }

    #region Model
    class BacNode
    {
        BacnetAddress adr;
        public uint device_id { get; set; }
        public BacNode(BacnetAddress adr, uint device_id)
        {
            this.adr = adr;
            this.device_id = device_id;
        }
        public BacnetAddress getAdd(uint device_id)
        {
            if (this.device_id == device_id)
                return adr;
            else
                return null;
        }
    }
    public class HaystackPointView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public string id { get; set; }
        public string dis { get; set; }
        public bool point { get; set; }
        public string equipRef { get; set; }
        public string siteRef { get; set; }
        public string kind { get; set; }
        public string unit { get; set; }
        public int presentValue { get; set; }
        public string tz { get; set; }
        public int DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string instanceID { get; set; }
        public bool sensor { get; set; }
        public string channelID { get; set; }
    }
    public class BacnetNewView_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string ObjectName { get; set; }
        public string InstanceID { get; set; }
        public int PresentValue { get; set; }
        public string Units { get; set; }
        public string MeshId { get; set; }
        public List<string> Zone { get; set; }
    }
    public class Bacnet_REC
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string ObjectName { get; set; }
        public string InstanceID { get; set; }
        public string PresentValue { get; set; }
        public int ObjectTypeID { get; set; }
        public string ObjectType { get; set; }
        public string Plugin { get; set; }
    }

    public class SmartlightSpecificZone
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string ObjectName { get; set; }
        public int Instance { get; set; }
        public int ObjectTypeID { get; set; }
        public string ObjectTypeName { get; set; }
        public int CategoryID { get; set; }
        public string Category { get; set; }
        public string Plugin { get; set; }
        public string Relay { get; set; }
        public string DimmingValue { get; set; }
        public string LifeTime { get; set; }
    }

    public class SmartlightLuxArea
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string ObjectName { get; set; }
        public int Instance { get; set; }
        public string PresentValue { get; set; }
        public int ObjectTypeID { get; set; }
        public string ObjectTypeName { get; set; }
        public int CategoryID { get; set; }
        public string Category { get; set; }
        public string Plugin { get; set; }
    }

    public class SmartlightLuxZone
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string ObjectName { get; set; }
        public int Instance { get; set; }
        public string PresentValue { get; set; }
        public int ObjectTypeID { get; set; }
        public string ObjectTypeName { get; set; }
        public int CategoryID { get; set; }
        public string Category { get; set; }
        public string Plugin { get; set; }
        public string Times { get; set; }
    }
    public class SmartlightActiveDevices
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string ObjectName { get; set; }
        public int Instance { get; set; }
        public string PresentValue { get; set; }
        public int ObjectTypeID { get; set; }
        public string ObjectTypeName { get; set; }
        public int CategoryID { get; set; }
        public string Category { get; set; }
        public string Plugin { get; set; }
    }

    public class SmartlightPowerDataLogs
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string ObjectName { get; set; }
        public int Instance { get; set; }
        public string Category { get; set; }
        public string Lifetime { get; set; }
        public string Power { get; set; }
        public string LightType { get; set; }
    }

    public class SmartlightSwitchedOffHourly
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string Location { get; set; }
        public string TotalOff { get; set; }
    }

    public class MailServer_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public DateTime recordTimestamp { get; set; }
        public int recordStatus { get; set; }
        public string emailAddress { get; set; }
        public string password { get; set; }
        public string title { get; set; }
        public string host { get; set; }
        public int port { get; set; }
        public string to { get; set; }
        public string cc { get; set; }
        public string bcc { get; set; }
    }
    public class AirconDataVAV_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public int DeviceID { get; set; }
        public string ObjectName { get; set; }
        public int InstanceID { get; set; }
        public int ObjectTypeID { get; set; }
        public string ObjectTypeName { get; set; }
        public string GroupVAV { get; set; }
        public string Title { get; set; }
        public string Unit { get; set; }
        public int Top { get; set; }
        public int Left { get; set; }
        public double PresentValue { get; set; }

    }

    public class CurtainObjectWhois_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public Int32 RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string ObjectName { get; set; }
        public int Instance { get; set; }
        public int ObjectTypeID { get; set; }
        public string ObjectTypeName { get; set; }
        public int CategoryID { get; set; }
        public CategoryView_REC Category { get; set; }
        public string Plugin { get; set; }
    }

    public class AllObjectWhoisSwitched_REC
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public Int32 RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string ObjectName { get; set; }
        public int Instance { get; set; }
        public int ObjectTypeID { get; set; }
        public string ObjectTypeName { get; set; }
        public int CategoryID { get; set; }
        public CategoryView_REC Category { get; set; }
        public string Plugin { get; set; }
        public string Location { get; set; }
    }

    public class CategoryView_REC
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
    }

    public class CurtainLogService
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public string DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string ObjectName { get; set; }
        public string InstanceID { get; set; }
        public string PresentValue { get; set; }
        public string Status { get; set; }
        public int ObjectTypeID { get; set; }
        public string ObjectType { get; set; }
        public string Plugin { get; set; }
    }

    public class SmartlightSwitchedOff
    {
        [BsonId]
        public ObjectId _id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RecordTimestamp { get; set; }
        public string DeviceID { get; set; }
        public string ObjectName { get; set; }
        public int Instance { get; set; }
        public string Location { get; set; }
        public int On { get; set; }
        public int Off { get; set; }
    }
    #endregion
}   
